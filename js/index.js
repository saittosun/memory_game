import '../css/style.scss';
import '@babel/polyfill';
import Memory from './component/Memory';

let level = 0;
const gameHolder = document.getElementById('memory_cardHolder');
// START NEXT LEVEL-WIN EVENT//
const startNextLevel = function() {
  gameHolder.innerHTML = '';

  // eslint-disable-next-line no-undef
  // if (level === 0 || memoryGame._score >= 50) {
  level += 1;
  const memoryGame = new Memory(level * 2, gameHolder, level);
  console.log(memoryGame._score);
  document.querySelector('.level__nr').innerHTML = `Level: ${level}`;
  // }
};

startNextLevel();

window.addEventListener('win', startNextLevel);

// START THE SAME LEVEL-LOSE EVENT//
const startSameLevel = function() {
  gameHolder.innerHTML = '';
  const memoryGame = new Memory(level * 2, gameHolder, level);
  document.querySelector('.level__nr').innerHTML = `Level: ${level}`;
};

startSameLevel();
window.addEventListener('lose', startSameLevel);

function clickCounter(e) {
  console.log(e.target.className === 'plus');
  if (e.target.className === 'plus' && (level >= 1 && level <= 4)) {
    level += 1;
  } else if (e.target.className === 'minus' && level > 1) {
    level -= 1;
  }
  document.querySelector('.level__nr').innerHTML = `Level: ${level}`;
  gameHolder.innerHTML = '';
  const memoryGame = new Memory(level * 2, gameHolder);
}

// function createGame2(e) {
//   if (level === 1) {
//     // eslint-disable-next-line no-new
//     const memoryGame = new Memory(level * 2, gameHolder);
//     e.target.style.backgroundColor = 'red';
//     e.target.removeEventListener('click', createGame2);
//     // const mem =...=> mem._tileRef.style.height = ''
//   }
// }
// RESET GAME//
function resetGame() {
  // eslint-disable-next-line no-restricted-globals
  location.reload();
}
document.querySelector('.reset').addEventListener('click', resetGame);

document.querySelector('.minus').addEventListener('click', clickCounter);
document.querySelector('.plus').addEventListener('click', clickCounter);

// const tiles = new Memory(8, document.getElementById('memory_cardHolder'));
// console.log(tiles._updateScore);
// console.log('---------------------hhhhhhhhhhhhhhhhhh');
// console.log(tiles._cardsArray);
// if (tiles._matchCount === 8) {
//   tiles();
// }
// const levelConfig = [
//   { levelNr: 1, totalCards: 8, iconSize: 'fa-5x' },
//   { levelNr: 2, totalCards: 12, iconSize: 'fa-4x' },
//   { levelNr: 3, totalCards: 16, iconSize: 'fa-3x' },
// ];
